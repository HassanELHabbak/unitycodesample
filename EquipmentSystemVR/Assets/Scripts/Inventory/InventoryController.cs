﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



//This class can easily be a singleton as it only has one instance of it
//However singletons are debated on topic so will just follow Unity's
//paradigm here and have it attached to a game object.
public class InventoryController : MonoBehaviour {
	
	private InventorySlot[] _slots;

	//This is a simplified value that holds what item
	//The player is about to equip
	[HideInInspector]
	public PickupItem PickupAvailable;

	public GameObject HeadSlotTransform;
	public GameObject LeftHandSlotTransform;
	public GameObject RightHandSlotTransform;


	public InventorySlot[] Slots{
		get{
			return _slots;
		}
	}

	// Use this for initialization
	void Start () {
		_slots = new InventorySlot[3];

		_slots[0] = new InventorySlot ();
		_slots[0].SlotName = Enum_EquipSlots.EquipSlots.lefthand;
		_slots[0].Empty = true;
		_slots[0].SlotPivot = LeftHandSlotTransform;
		_slots[0].FireButton = "LeftHand";
		_slots[0].SlotNumber = 0;

		_slots[1] = new InventorySlot ();
		_slots[1].SlotName = Enum_EquipSlots.EquipSlots.righthand;
		_slots[1].Empty = true;
		_slots[1].SlotPivot = RightHandSlotTransform;
		_slots[1].FireButton = "RightHand";
		_slots[1].SlotNumber = 1;

		_slots[2] = new InventorySlot ();
		_slots[2].SlotName = Enum_EquipSlots.EquipSlots.head;
		_slots[2].Empty = true;
		_slots[2].SlotPivot = HeadSlotTransform;
		_slots[2].FireButton = "";
		_slots[2].SlotNumber = 2;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnGUI() {
		GUI.Label(new Rect(Screen.width/2-30, 10, 100, 20), _slots[2].LabelText);
		GUI.Label (new Rect(Screen.width - 80,Screen.height-35,200,80), _slots[1].LabelText); 
		GUI.Label (new Rect(15,Screen.height-35,200,80), _slots[0].LabelText); 
	}

	public Enum_InventoryMessage.Message EquipItem(Enum_EquipSlots.EquipSlots slotToEquipOn){

		int slotNumber = (int)slotToEquipOn;

		if (PickupAvailable == null) {
			return Enum_InventoryMessage.Message.noItemFound;
		}

		if (!PickupAvailable.EquippableSlots.Contains (slotToEquipOn)) {
			return Enum_InventoryMessage.Message.wrongItemSlot;
		}

		UnequipItem (slotNumber);

		GameObject newItem = (GameObject)Instantiate(Resources.Load(PickupAvailable.PrefabName));
		newItem.transform.parent = _slots [slotNumber].SlotPivot.transform;
		newItem.transform.localPosition = new Vector3 (0,0,0);
		_slots [slotNumber].inventoryItem = newItem.GetComponent<InventoryItem> ();
		_slots [slotNumber].LabelText = _slots [slotNumber].inventoryItem.LabelText;
		_slots [slotNumber].Empty = false;

		_slots [slotNumber].inventoryItem.slotEquippedOn = _slots [slotNumber];
		_slots [slotNumber].inventoryItem.inventoryController = this;
		_slots [slotNumber].inventoryItem.FireKeyBinding = _slots [slotNumber].FireButton;


		return Enum_InventoryMessage.Message.itemEquipped;
	}

	public void UnequipItem(int slotNumber){
		InventorySlot slot = _slots [slotNumber];

		//If the slot is not already empty
		if (!slot.Empty) {
			//Now we fire the item's removal function. This approach does not account for animation times
			//etc. If we are to allow for this, we would then use delegates and animation notifications
			//to account for events sequence.
			slot.LabelText = "Empty";
			slot.Empty = true;
			slot.inventoryItem.UnEquip();

		}
	}

	private bool isSlotEmpty(int slotNumber){
		if (slotNumber > (_slots.Length-1)) {
			return false;
		}

		return _slots [slotNumber].Empty;
	}
}