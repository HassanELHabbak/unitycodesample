﻿using UnityEngine;
using System.Collections;

public class InventorySlot {
	public Enum_EquipSlots.EquipSlots SlotName { get; set; }
	public bool Empty{ get; set; }
	public InventoryItem inventoryItem;
	public string LabelText = "Empty";
	public GameObject SlotPivot;
	public string FireButton = "";
	public int SlotNumber;
}
