﻿using UnityEngine;
using System.Collections;

static public class Enum_InventoryMessage {

	public enum Message
	{
		itemEquipped, wrongItemSlot, noItemFound
	}
}
