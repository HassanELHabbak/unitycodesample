﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class InventoryItem : MonoBehaviour {

	protected string _itemName;
	protected string _fireKeyBinding;
	protected string _labelText;

	public InventoryController inventoryController;
	public InventorySlot slotEquippedOn;

	//----Getters and setters
	public string ItemName {
		get{
			return _itemName;
		}
	}

	public string FireKeyBinding {
		set{
			_fireKeyBinding = value;
		}
	}

	public string LabelText {
		get{
			return _labelText;
		}
	}
	
	//----Common Methods
	public abstract void UnEquip ();
}


