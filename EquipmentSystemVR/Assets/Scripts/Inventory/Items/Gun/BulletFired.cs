﻿using UnityEngine;
using System.Collections;

public class BulletFired : MonoBehaviour {

	public int speed = 3000;

	// Use this for initialization
	void Start () {
		rigidbody.AddForce (transform.forward * speed);
		Invoke ("destroyBullet", 5f);
	}
	
	void destroyBullet(){
		GameObject.Destroy (this.gameObject);
	}
}
