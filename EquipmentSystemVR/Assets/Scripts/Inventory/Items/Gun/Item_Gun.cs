﻿using UnityEngine;
using System.Collections;

public class Item_Gun : InventoryItem {

	public GameObject BulletPrefab;

	public Transform BulletsSpawnPoint;
	
	private GunClipLoaded _gunClip;

	void Awake () {
		_itemName = "Gun";
		_labelText = "Gun";
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		//If the fire button has been pressed  
		if(Input.GetButtonDown(_fireKeyBinding))  
		{  
			//Shoot the bullet  
			Fire();  
			//Cancel any Shoot() method code execution  
			CancelInvoke("Fire");  
		}  
		
		//while the fire button is being held down  
		if(Input.GetButton(_fireKeyBinding) && !IsInvoking("Fire"))  
		{  
			//Execute the Shoot() method in the next 0.2 seconds.  
			Invoke("Fire",0.2f);  
		}  
		
		//If the fire has been released, cancel any scheduled Shoot() method executions  
		if(Input.GetButtonUp(_fireKeyBinding))  
		{  
			//Cancel any Shoot() method code execution  
			CancelInvoke("Fire");  
		}  
	}

	private void Fire(){

		if (_gunClip == null) 
		{
			_gunClip = this.gameObject.GetComponent<GunClipLoaded> ();
		}
		 
		if (_gunClip != null) 
		{
			_gunClip.UnloadOneBullet();
			ShootOneBullet();
		}
	}

	private void ShootOneBullet(){
		GameObject bullet = (GameObject)GameObject.Instantiate (BulletPrefab, BulletsSpawnPoint.position, BulletsSpawnPoint.rotation);
	}

	public override void UnEquip (){
		GameObject.Destroy (this.gameObject);
	}
}
