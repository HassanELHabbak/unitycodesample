﻿using UnityEngine;
using System.Collections;

public class Item_AmmoClip : InventoryItem {

	void Awake () {
		_itemName = "AmmoClip";
		_labelText = "AmmoClip";
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown(_fireKeyBinding))  
		{  
			foreach(InventorySlot slot in inventoryController.Slots){
				if(!slot.Empty)
				{
					if(slot.inventoryItem.ItemName == "Gun")
					{
						EquipToGun(slot.inventoryItem);
						break;
					}
				}
			}
		} 
	}

	private void EquipToGun(InventoryItem gun){
		GunClipLoaded clipLoadedOnGun = gun.gameObject.GetComponent<GunClipLoaded>();

		if (clipLoadedOnGun != null) {
			clipLoadedOnGun.Unequip();
		}

		gun.gameObject.AddComponent ("GunClipLoaded");

		inventoryController.UnequipItem (slotEquippedOn.SlotNumber);
	}
	
	public override void UnEquip (){
		GameObject.Destroy (this.gameObject);
	}
}
