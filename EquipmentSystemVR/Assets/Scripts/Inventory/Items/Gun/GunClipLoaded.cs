﻿using UnityEngine;
using System.Collections;

public class GunClipLoaded : MonoBehaviour {
	
	protected int _ammoCount = 30;

	public int AmmoCount {
		get{
			return _ammoCount;
		}
	}

	public void UnloadOneBullet() {
		_ammoCount--;
		Debug.Log ("Bullets remaining "+_ammoCount);

		if (_ammoCount <= 0) {
			Unequip();
		}
	}

	public void Unequip() {
		Destroy (this);
	}

}
