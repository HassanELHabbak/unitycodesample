﻿using UnityEngine;
using System.Collections;

public class Item_Hat : InventoryItem {
	
	void Awake () {
		_itemName = "Hat";
		_labelText = "Hat";
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	public override void UnEquip (){
		GameObject.Destroy (this.gameObject);
	}
}
