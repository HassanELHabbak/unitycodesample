﻿using UnityEngine;
using System.Collections;

public class Item_FlashLight : InventoryItem {

	public GameObject LightPart;
	private bool _lightActive = false;

	void Awake () {
		_itemName = "Flashlight";
		_labelText = "Flashlight";
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown (_fireKeyBinding) ) {
			Debug.Log("Fire!! "+ _fireKeyBinding);

			if(_lightActive)
			{
				_lightActive = false;
				LightPart.renderer.material.color = Color.white;
			}
			else
			{
				_lightActive = true;
				LightPart.renderer.material.color = Color.red;
			}
		}
	}
	
	public override void UnEquip (){
		GameObject.Destroy (this.gameObject);
	}
}
