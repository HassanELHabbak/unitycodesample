﻿using UnityEngine;
using System.Collections;

public class Item_Rock : InventoryItem {

	private bool _projectileFired = false;
	public int thrust = 1000;
	
	void Awake () {
		_itemName = "Rock";
		_labelText = "Rock";
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown (_fireKeyBinding) && !_projectileFired) {
			Debug.Log("Fire!! "+ _fireKeyBinding);
			FireProjectile();
		}
	}

	private void FireProjectile(){
		_projectileFired = true;
		this.gameObject.rigidbody.isKinematic = false;
		this.gameObject.transform.parent = null;
		this.gameObject.rigidbody.AddForce (transform.forward * thrust);

		inventoryController.UnequipItem (slotEquippedOn.SlotNumber);
		StartCoroutine ("DestroyProjectileAfterDelay");
	}

	IEnumerator DestroyProjectileAfterDelay() {
		yield return new WaitForSeconds (5);
		GameObject.Destroy (this.gameObject);
	}

	//--Overrides
	public override void UnEquip (){
		if (!_projectileFired) {
			GameObject.Destroy (this.gameObject);
		}
	}
}
