﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

	public InventoryController inventoryController;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("EquipLeftHand")) {
			Debug.Log("Left Hand Equip");
			Enum_InventoryMessage.Message message = inventoryController.EquipItem(Enum_EquipSlots.EquipSlots.lefthand);
			Debug.Log("Inventory Message: "+message);
		}

		if (Input.GetButtonDown ("EquipRightHand")) {
			Enum_InventoryMessage.Message message = inventoryController.EquipItem(Enum_EquipSlots.EquipSlots.righthand);
			Debug.Log("Inventory Message: "+message);
		}

		if (Input.GetButtonDown ("EquipHead")) {
			Enum_InventoryMessage.Message message = inventoryController.EquipItem(Enum_EquipSlots.EquipSlots.head);
			Debug.Log("Inventory Message: "+message);
		}



		//Key 1 default
		if (Input.GetButtonDown ("Gun")) {
			Debug.Log("Input Gun");
			inventoryController.PickupAvailable =(Pickup_Gun)ScriptableObject.CreateInstance(typeof(Pickup_Gun));
			inventoryController.PickupAvailable.setInventoryController(inventoryController);
		}

		//key 2 default
		if (Input.GetButtonDown ("AmmoClip")) {
			Debug.Log("Input AmmoClip");
			inventoryController.PickupAvailable =(Pickup_AmmoClip)ScriptableObject.CreateInstance(typeof(Pickup_AmmoClip));
			inventoryController.PickupAvailable.setInventoryController(inventoryController);
		}


		//key 3 default
		if (Input.GetButtonDown ("FlashLight")) {
			Debug.Log("Input FlashLight");
			inventoryController.PickupAvailable =(Pickup_FlashLight)ScriptableObject.CreateInstance(typeof(Pickup_FlashLight));
			inventoryController.PickupAvailable.setInventoryController(inventoryController);
		}

		//key 4 default
		if (Input.GetButtonDown ("Rock")) {
			Debug.Log("Input Rock");
			inventoryController.PickupAvailable =(Pickup_Rock)ScriptableObject.CreateInstance(typeof(Pickup_Rock));
			inventoryController.PickupAvailable.setInventoryController(inventoryController);
		}

		//key 5 default
		if (Input.GetButtonDown ("Hat")) {
			Debug.Log("Input Hat");
			inventoryController.PickupAvailable =(Pickup_Hat)ScriptableObject.CreateInstance(typeof(Pickup_Hat));
			inventoryController.PickupAvailable.setInventoryController(inventoryController);
		}
	}
}
