﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pickup_Rock : PickupItem {
	
	// Use this for initialization
	void OnEnable () {
		_itemName = "Rock";
		_prefabName = "P_Rock";
		
		_equippableSlots = new List<Enum_EquipSlots.EquipSlots> ();
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.lefthand);
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.righthand);
	}
	
	public override void PickUp (){
	}
}