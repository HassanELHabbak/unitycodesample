﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pickup_FlashLight : PickupItem {
	
	// Use this for initialization
	void OnEnable () {
		_itemName = "Flashlight";
		_prefabName = "P_FlashLight";
		
		_equippableSlots = new List<Enum_EquipSlots.EquipSlots> ();
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.lefthand);
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.righthand);
	}
	
	public override void PickUp (){
	}
}
