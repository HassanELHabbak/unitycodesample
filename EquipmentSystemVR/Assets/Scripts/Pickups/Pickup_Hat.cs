﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pickup_Hat : PickupItem {
	
	// Use this for initialization
	void OnEnable () {
		_itemName = "Hat";
		_prefabName = "P_Hat";
		
		_equippableSlots = new List<Enum_EquipSlots.EquipSlots> ();
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.lefthand);
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.righthand);
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.head);
	}
	
	public override void PickUp (){
	}
}
