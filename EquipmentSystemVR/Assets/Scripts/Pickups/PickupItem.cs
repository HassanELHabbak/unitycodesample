﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//In a real application, this class should be monobehaviour
//It also should be a prefab and exists in the world.
//It should also have a reference to the prefab it is supposed
//to instantiate
public abstract class PickupItem : ScriptableObject {

	protected string _itemName;
	protected string _prefabName;
	protected List<Enum_EquipSlots.EquipSlots> _equippableSlots;
	protected InventoryController _inventoryController; 

	
	//----Properties
	public string ItemName {
		get{
			return _itemName;
		}
	}

	public string PrefabName {
		get{
			return _prefabName;
		}
	}
	
	public List<Enum_EquipSlots.EquipSlots> EquippableSlots {
		get{
			return _equippableSlots;
		}
	}

	public void setInventoryController(InventoryController inventoryController){
		_inventoryController = inventoryController;
	}
	

	//This function is to be fired when the item is picked up
	//In a real application this is where pickup effects and
	//animations goes.
	public abstract void PickUp ();
}
