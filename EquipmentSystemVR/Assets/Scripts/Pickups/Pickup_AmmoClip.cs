﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pickup_AmmoClip : PickupItem {
	
	// Use this for initialization
	void OnEnable () {
		_itemName = "AmmoClip";
		_prefabName = "P_AmmoClip";
		
		_equippableSlots = new List<Enum_EquipSlots.EquipSlots> ();
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.lefthand);
		_equippableSlots.Add (Enum_EquipSlots.EquipSlots.righthand);
	}
	
	public override void PickUp (){
	}
}
